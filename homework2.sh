[koldre@localhost ~]$ git push
fatal: not a git repository (or any of the parent directories): .git
[koldre@localhost ~]$ clear
[koldre@localhost ~]$ [koldre@localhost ~]$ cat homework2.sh
#!/bin/bash

user_input="$1"
echo="user entered input:  $user_input"
length="$(echo -n "$user_input" | wc -c | tr -d ' ')"

if [ "$length" -eq 3 ]
then
        echo ""
        echo " Converting $user_input to sybolic"
        echo
        echo
        for loop in 1 2 3
        do
                #need to cut user_input apart into 3 pieces
                # and then assign the echo statment wiht the loop
                # to put them back together by suppressing line feed
                position="$(echo "$user_input" | cut -c $loop )"

                case $position in
                        0 ) converted="---";;
                        1 ) converted="--x";;
                        2 ) converted="-w-";;
                        3 ) converted="-wx";;
                        4 ) converted="r--";;
                        5 ) converted="r-x";;
                        6 ) converted="rw-";;
                        7 ) converted="rwx";;
                esac
        echo -e "$converted\c"
        done

elif [ "$length" -eq 9 ]
then
        echo
        echo " Converting $user_input to Octal "
        echo
        for loop in 1,2,3 4,5,6 7,8,9
        do
                position="$(echo "$user_input" | cut -c $loop )"

                case $position in
                        --- ) converted=0;;
                        --x ) converted=1;;
                        -w- ) converted=2;;
                        -wx ) converted=3;;
                        r-- ) converted=4;;
                        r-x ) converted=5;;
                        rw- ) converted=6;;
                        rwx ) converted=7;;
                esac
        echo -e "$converted\c"
done
else
        echo
        echo "User error.   Try again"
fi
echo
echo "wow"
echo